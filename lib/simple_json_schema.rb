# frozen_string_literal: true

require 'simple_json_schema/version'

require 'net/http'

require 'active_support/core_ext/hash'
require 'addressable/template'
require 'ecma-re-validator'
require 'regexp_parser'

require 'simple_json_schema/validators/concerns/format'
require 'simple_json_schema/validators/base'
require 'simple_json_schema/validators/numeric'
require 'simple_json_schema/validators/boolean'
require 'simple_json_schema/validators/integer'
require 'simple_json_schema/validators/null'
require 'simple_json_schema/validators/number'
require 'simple_json_schema/validators/string'

require 'simple_json_schema/concerns/hash_acessor'
require 'simple_json_schema/uri_extender'
require 'simple_json_schema/cache'
require 'simple_json_schema/checker'
require 'simple_json_schema/items_helper'
require 'simple_json_schema/properties_helper'
require 'simple_json_schema/regex_helper'
require 'simple_json_schema/ref_helper'
require 'simple_json_schema/scope'
require 'simple_json_schema/validator'
require 'simple_json_schema/custom_draft7'

module SimpleJSONSchema
  class UnsupportedMetaSchema < StandardError; end

  DEFAULT_SCHEMA = 'https://gitlab.com/vliper/simple_json_schema/-/raw/master/draft-07-schema#'
  DRAFT_BY_SCHEMA = {
    DEFAULT_SCHEMA => :draft7,
    'http://json-schema.org/draft-07/schema#' => :draft
  }.freeze

  NET_HTTP_REF_RESOLVER = proc { |uri| JSON.parse(Net::HTTP.get(uri)) }

  class << self
    def valid?(data, schema, options = nil)
      errors = valid(data, schema, options)
      errors.none?
    end

    def valid(data, schema, options = nil)
      if schema.is_a?(Hash)
        schema = schema.with_indifferent_access
        options ||= schema.delete(:options)
      end

      options = options.with_indifferent_access if options.is_a?(Hash)

      scope = Scope.new(data: data, schema: schema, draft: draft_class(schema), options: options)
      Validator.validate(scope)
      scope.errors
    rescue StandardError => e
      scope.error(:invalid, exception: e.message)
    end

    private

    def draft_class(schema)
      meta_schema = schema.is_a?(Hash) && schema.key?('$schema') ? schema['$schema'] : DEFAULT_SCHEMA
      DRAFT_BY_SCHEMA[meta_schema] || raise(UnsupportedMetaSchema, meta_schema)
    end
  end
end
