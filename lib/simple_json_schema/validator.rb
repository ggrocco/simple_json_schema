# frozen_string_literal: true

module SimpleJSONSchema
  class Validator
    VALIDATORES = {
      'null' => Validators::Null.new,
      'boolean' => Validators::Boolean.new,
      'integer' => Validators::Integer.new,
      'number' => Validators::Number.new,
      'string' => Validators::String.new
    }.freeze

    class << self
      def validate(scope)
        return unless scope.segment?
        return if validate_ref(scope)

        Checker.enum(scope)
        Checker.const(scope)

        validate_correlations(scope)
        validate_by_type(scope)
      end

      private

      def validate?(scope)
        scope.errors = []

        validate(scope.no_hooks)
        scope.errors.none?
      end

      def validate_ref(scope)
        ref_pointer = RefHelper.pointer(scope)
        return if ref_pointer.nil?

        new_scope = scope.merge(schema_paths: ref_pointer.ref_paths, parent_uri: ref_pointer.parent_uri)
        new_scope.segment = ref_pointer.segment unless ref_pointer.segment.nil?
        validate(new_scope.path_to(schema_path: ref_pointer.segment_paths))
        true
      end

      def validate_correlations(scope)
        validate_all_of(scope)
        validate_any_of(scope)
        validate_one_of(scope)
        validate_not(scope)
        validate_if_then_else(scope)
      end

      def validate_all_of(scope)
        scope[:allOf]&.each_index do |index|
          validate(scope.path_to(schema_path: [:allOf, index]).no_hooks)
        end
      end

      def validate_any_of(scope)
        any_of = scope[:anyOf]
        return if any_of.nil?
        return if any_of.each_index.any? { |index| validate?(scope.path_to(schema_path: [:anyOf, index])) }

        any_of.each_index { |index| validate(scope.path_to(schema_path: [:anyOf, index]).no_hooks) }
      end

      def validate_one_of(scope)
        one_of = scope[:oneOf]
        return if one_of.nil?

        valid_count = one_of.each_index.count { |index| validate?(scope.path_to(schema_path: [:oneOf, index])) }

        if valid_count > 1
          scope.error(:oneOf)
        elsif valid_count.zero?
          one_of.each_index { |index| validate(scope.path_to(schema_path: [:oneOf, index]).no_hooks) }
        end
      end

      def validate_not(scope)
        return unless scope.key?(:not)

        scope.error(:not) if validate?(scope.path_to(schema_path: :not))
      end

      def validate_if_then_else(scope)
        return unless scope.key?(:if)

        if validate?(scope.path_to(schema_path: :if))
          validate(scope.path_to(schema_path: :then)) if scope.key?(:then)
        elsif scope.key?(:else)
          validate(scope.path_to(schema_path: :else))
        end
      end

      def validate_by_type(scope)
        case scope.type
        when 'object'
          valid_object(scope)
        when 'array'
          valid_array(scope)
        when Array
          scope.error(:type) unless scope.type.any? { |type| validate?(scope.path_to(type: type)) }
        else
          validate_base_types(scope)
        end
      end

      def validate_base_types(scope)
        VALIDATORES[scope.type]&.valid(scope)
      end

      def valid_object(scope)
        return scope.error(:object) unless scope.value.is_a?(Hash)

        PropertiesHelper.processe_defualt(scope)
        PropertiesHelper.checkers(scope)

        valid_object_dependencies(scope)
        valid_object_property_names(scope)
        valid_object_properties(scope)
      end

      def valid_object_dependencies(scope)
        PropertiesHelper.each_dependency(scope) do |schema_path|
          new_scope = scope.path_to(schema_path: schema_path)
          new_scope.segment = { required: new_scope.segment } if new_scope.segment.is_a?(Array)
          validate(new_scope)
        end
      end

      def valid_object_property_names(scope)
        PropertiesHelper.each_property_name(scope) do |property_name|
          validate(scope.path_to(schema_path: :propertyNames).replace_data(property_name))
        end
      end

      def valid_object_properties(scope)
        scope.value.each_key do |property_name|
          PropertiesHelper.map_property_schema_path(scope, property_name).each do |schema_path|
            new_scope = scope.path_to(schema_path: schema_path, data_path: property_name)
            new_scope.around_hooks do
              validate(new_scope)
            end
          end
        end
      end

      def valid_array(scope)
        value = scope.value
        return scope.error(:array) unless value.is_a?(Array)

        ItemsHelper.checkers(scope)

        ItemsHelper.contains_in_items?(scope) do |index|
          validate?(scope.path_to(schema_path: :contains, data_path: index))
        end

        ItemsHelper.each_path(scope) do |path, index|
          validate(scope.path_to(schema_path: path, data_path: index))
        end
      end
    end
  end
end
