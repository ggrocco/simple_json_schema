# frozen_string_literal: true

module SimpleJSONSchema
  module RegexHelper
    RUBY_REGEX_ANCHORS_TO_ECMA_262 = {
      bos: 'A',
      eos: 'z',
      bol: '\A',
      eol: '\z'
    }.freeze

    class << self
      def ecma_262_regex(pattern, cache)
        cache.fetch(pattern) do
          Regexp.new(
            Regexp::Scanner.scan(pattern).map do |type, token, text|
              type == :anchor ? RUBY_REGEX_ANCHORS_TO_ECMA_262.fetch(token, text) : text
            end.join
          )
        end
      end
    end
  end
end
