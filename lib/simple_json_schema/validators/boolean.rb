# frozen_string_literal: true

module SimpleJSONSchema
  module Validators
    class Boolean < Base
      TRUE_VALUES  = ['true', '1', 1, 'yes'].freeze
      FALSE_VALUES = ['false', '0', 0, 'no'].freeze

      BOOLEANS = Set[true, false].freeze

      def validate(scope)
        value = scope.value

        return scope.error(:boolean) unless BOOLEANS.include?(value)
      end

      def casting(value)
        return true if TRUE_VALUES.include?(value)
        return false if FALSE_VALUES.include?(value)
      end
    end
  end
end
