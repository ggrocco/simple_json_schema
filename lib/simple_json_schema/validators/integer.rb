# frozen_string_literal: true

module SimpleJSONSchema
  module Validators
    class Integer < Numeric
      INTEGER_FORMAT = /\A-?\d+\Z/.freeze

      def validate(scope)
        value = scope.value

        return scope.error(:integer) if !value.is_a?(::Numeric) || (!value.is_a?(::Integer) && value.floor != value)
        return scope.error(:zero_not_allowed) if scope[:not_zero] == true && value.zero?

        super
      end

      def casting(value)
        value.to_i if value.is_a?(::String) && INTEGER_FORMAT.match?(value)
      end
    end
  end
end
