# frozen_string_literal: true

module SimpleJSONSchema
  module Validators
    class String < Base
      include Concerns::Format

      def validate(scope)
        value = scope.value

        return scope.error(:string) unless value.is_a?(::String)
        return scope.error(:blank) if scope[:not_blank] == true && value.blank?

        Checker.at_size(scope, :maxLength, :>)
        Checker.at_size(scope, :minLength, :<)

        valid_pattern(scope)
        valid_format(scope)
      end

      private

      def valid_pattern(scope)
        pattern = scope[:pattern]
        return if pattern.nil?

        match = RegexHelper.ecma_262_regex(pattern, scope.cache).match?(scope.value)
        return if match

        scope.error(:pattern)
      end
    end
  end
end
