# frozen_string_literal: true

module SimpleJSONSchema
  module Validators
    class Numeric < Base
      def validate(scope)
        value = scope.value

        Checker.at_value(scope, :maximum, :>)
        Checker.at_value(scope, :minimum, :<)
        Checker.at_value(scope, :exclusiveMaximum, :>=)
        Checker.at_value(scope, :exclusiveMinimum, :<=)

        multiple_of = scope[:multipleOf]
        return unless multiple_of

        quotient = value / multiple_of.to_f
        scope.error(:multipleOf) unless quotient.floor == quotient
      end
    end
  end
end
