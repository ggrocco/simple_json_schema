# frozen_string_literal: true

module SimpleJSONSchema
  module Validators
    class Base
      def valid(scope)
        cast(scope) if scope.options[:cast] == true
        validate(scope)
      end

      private

      def validate(scope)
        raise NotImplementedError
      end

      def cast(scope)
        value = casting(scope.value)
        scope.value = value unless value.nil? && scope.value != value
      end

      def casting(value); end
    end
  end
end
