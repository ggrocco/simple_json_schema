# frozen_string_literal: true

module SimpleJSONSchema
  module Validators
    class Null < Base
      def validate(scope)
        scope.error(:null) unless scope.value.nil?
      end
    end
  end
end
