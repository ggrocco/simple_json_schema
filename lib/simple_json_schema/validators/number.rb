# frozen_string_literal: true

module SimpleJSONSchema
  module Validators
    class Number < Numeric
      FLOAT_FORMAT = /\A-?\d+\.\d+\Z/.freeze

      def validate(scope)
        value = scope.value

        return scope.error(:number) unless value.is_a?(::Numeric)
        return scope.error(:zero_not_allowed) if scope[:not_zero] == true && value.zero?

        super
      end

      def casting(value)
        BigDecimal(value) if value.is_a?(::String) && FLOAT_FORMAT.match?(value)
      end
    end
  end
end
