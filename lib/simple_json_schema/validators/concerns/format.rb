# frozen_string_literal: true

module SimpleJSONSchema
  module Validators
    module Concerns
      module Format
        # this is no good
        EMAIL_REGEX = /\A[^@\s]+@([\p{L}\d-]+\.)+[\p{L}\d\-]{2,}\z/i.freeze
        LABEL_REGEX_STRING = '[\p{L}\p{N}]([\p{L}\p{N}\-]*[\p{L}\p{N}])?'
        HOSTNAME_REGEX = /\A(#{LABEL_REGEX_STRING}\.)*#{LABEL_REGEX_STRING}\z/i.freeze
        DATE_TIME_OFFSET_REGEX = /(Z|[+\-]([01][0-9]|2[0-3]):[0-5][0-9])\z/i.freeze
        INVALID_QUERY_REGEX = /[[:space:]]/.freeze
        ASCII_REGEX = /[^[:ascii:]]/.freeze

        def valid_format(scope)
          format = scope[:format]
          return if format.nil?
          return if valid_spec_format?(scope.value, format)

          scope.error(:format)
        end

        private

        # rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/MethodLength
        def valid_spec_format?(value, format)
          case format
          when 'date-time'
            valid_date_time?(value)
          when 'date'
            valid_date_time?("#{value}T04:05:06.123456789+07:00")
          when 'time'
            valid_date_time?("2001-02-03T#{value}")
          when 'email'
            value.ascii_only? && valid_email?(value)
          when 'idn-email'
            valid_email?(value)
          when 'hostname'
            value.ascii_only? && valid_hostname?(value)
          when 'idn-hostname'
            valid_hostname?(value)
          when 'ipv4'
            valid_ip?(value, :v4)
          when 'ipv6'
            valid_ip?(value, :v6)
          when 'uri'
            valid_uri?(value)
          when 'uri-reference'
            valid_uri_reference?(value)
          when 'iri'
            valid_uri?(iri_escape(value))
          when 'iri-reference'
            valid_uri_reference?(iri_escape(value))
          # when 'uri-template'
          #   valid_uri_template?(value)
          when 'json-pointer'
            Checker.json_pointer?(value)
          when 'relative-json-pointer'
            Checker.relative_json_pointer?(value)
          when 'regex'
            EcmaReValidator.valid?(value)
          end
        end
        # rubocop:enable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/MethodLength

        def valid_date_time?(value)
          DateTime.rfc3339(value)
          DATE_TIME_OFFSET_REGEX.match?(value)
        rescue ArgumentError => e
          raise e unless e.message == 'invalid date'

          false
        end

        def valid_email?(value)
          EMAIL_REGEX.match?(value)
        end

        def valid_hostname?(value)
          HOSTNAME_REGEX.match?(value) && value.split('.').all? { |label| label.size <= 63 }
        end

        def valid_ip?(value, type)
          ip_address = IPAddr.new(value)
          type == :v4 ? ip_address.ipv4? : ip_address.ipv6?
        rescue IPAddr::InvalidAddressError
          false
        end

        def parse_uri_scheme(value)
          scheme, _, _, _, _, _, opaque, query, = URI::RFC3986_PARSER.split(value)
          # URI::RFC3986_PARSER.parse allows spaces in these and I don't think it should
          raise URI::InvalidURIError if INVALID_QUERY_REGEX.match?(query) || INVALID_QUERY_REGEX.match?(opaque)

          scheme
        end

        def valid_uri?(value)
          !parse_uri_scheme(value).nil?
        rescue URI::InvalidURIError
          false
        end

        def valid_uri_reference?(value)
          parse_uri_scheme(value)
          true
        rescue URI::InvalidURIError
          false
        end

        def iri_escape(value)
          value.gsub(ASCII_REGEX) do |match|
            us = match
            tmp = +''
            us.each_byte do |uc|
              tmp << format('%%%02X', uc)
            end
            tmp
          end.force_encoding(Encoding::US_ASCII)
        end

        # def valid_uri_template?(value)
        #   Addressable::Template.new(value)
        #   true
        # rescue Addressable::InvalidURIError
        #   false
        # end
      end
    end
  end
end
