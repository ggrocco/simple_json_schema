# frozen_string_literal: true

module SimpleJSONSchema
  class Scope
    extend Concerns::HashAccessor

    CLASS_TYPE_TRANSLATE = {
      Array => 'array',
      Float => 'number',
      Hash => 'object',
      Integer => 'integer',
      Numeric => 'number',
      String => 'string'
    }.freeze

    hash_accessor :scope, %i[data schema type draft data_paths schema_paths errors parent_uri ids options]

    def initialize(**args)
      scope.merge!(args)

      self.data_paths ||= []
      self.schema_paths ||= []
      self.errors ||= []
      self.ids ||= {}
      self.options ||= {}
      self.type ||= evaluate_type
    end

    def path_to(data_path: nil, schema_path: nil, type: nil)
      return self if data_path.nil? && schema_path.nil? && type.nil?

      new_data_paths = data_paths + [data_path].flatten.compact
      new_schema_paths = schema_paths + [schema_path].flatten.compact

      merge(data_paths: new_data_paths, schema_paths: new_schema_paths, type: type)
    end

    def merge(data_paths: self.data_paths, schema_paths: self.schema_paths, type: nil, parent_uri: nil)
      self.class.new(**scope.merge(data_paths: data_paths,
                                   schema_paths: schema_paths,
                                   type: type,
                                   parent_uri: parent_uri || URIExtender.join_uri(self.parent_uri, id)))
    end

    def replace_data(new_data)
      self.data = new_data
      self.data_paths = []
      self.type = evaluate_type
      self
    end

    def error(type, details = nil)
      error = {
        type: type,
        segment: segment,
        value: value,
        data_pointer: "/#{data_paths.join('/')}",
        schema_pointer: "/#{schema_paths.join('/')}"
      }

      error[:details] = details if details

      errors.push(error)
    end

    def no_hooks
      self.options = options.except(:before_property_validation, :after_property_validation)
      self
    end

    def value
      dig(data, data_paths)
    end

    def value=(new_value)
      return if errors.any? # only convert value until be invalid.

      replace(data, data_paths, new_value) { self.data = new_value }
    end

    def segment
      dig(schema, schema_paths)
    end

    def segment=(new_segment)
      replace(schema, schema_paths, new_segment) { self.schema = new_segment }
    end

    def segment?
      if segment == false
        error('schema')
        return false
      end

      !(segment == true || segment.nil?)
    end

    def id
      self[:$id]
    end

    def [](key)
      return unless segment.is_a?(Hash)

      segment[key]
    end

    def key?(key)
      segment.is_a?(Hash) && segment.key?(key)
    end

    def cache
      options[:cache] ||= Cache.new
    end

    def around_hooks
      options[:before_property_validation]&.call(self)
      yield
      options[:after_property_validation]&.call(self)
    end

    def loaded_ids
      if ids.empty?
        resolve_ids(ids, schema)
        ids[:$loaded] = true
      end
      ids
    end

    private

    def evaluate_type
      if key?(:type)
        self[:type]
      else
        CLASS_TYPE_TRANSLATE[value.class]
      end
    end

    def dig(hash, paths)
      return hash if paths.empty?

      hash.dig(*paths) if hash.respond_to?(:dig)
    rescue TypeError
      nil
    end

    def replace(hash, paths, value)
      *steps, leaf = paths

      if steps.empty?
        if leaf.nil?
          yield if block_given?
        else
          hash[leaf] = value
        end
      else
        hash.dig(*steps)[leaf] = value
      end
    end

    def resolve_ids(ids, schema, parent_uri = nil, schema_paths = [])
      case schema
      when Array
        schema.each_with_index { |subschema, index| resolve_ids(ids, subschema, parent_uri, schema_paths + [index]) }
      when Hash
        uri = URIExtender.join_uri(parent_uri, schema[:$id])

        schema.each do |key, value|
          ids[uri.to_s] = { schema: schema, schema_paths: schema_paths } if key == '$id' && uri != parent_uri

          resolve_ids(ids, value, uri, schema_paths + [key])
        end
      end
    end
  end
end
