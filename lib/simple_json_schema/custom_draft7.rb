# frozen_string_literal: true

module SimpleJSONSchema
  # Draft-7 whit custom settings.
  CUSTOM_DRAFT7 = {
    '$schema' => 'https://gitlab.com/vliper/simple_json_schema/-/raw/master/draft-07-schema#',
    '$id' => 'https://gitlab.com/vliper/simple_json_schema/-/raw/master/draft-07-schema#',
    'title' => 'Simple JSON Schema meta-schema',
    'definitions' => {
      'schemaArray' => {
        'type' => 'array',
        'minItems' => 1,
        'items' => { '$ref' => '#' }
      },
      'nonNegativeInteger' => {
        'type' => 'integer',
        'minimum' => 0
      },
      'nonNegativeIntegerDefault0' => {
        'allOf' => [
          { '$ref' => '#/definitions/nonNegativeInteger' },
          { 'default' => 0 }
        ]
      },
      'simpleTypes' => {
        'enum' => %w[
          array
          boolean
          integer
          null
          number
          object
          string
        ]
      },
      'stringArray' => {
        'type' => 'array',
        'items' => { 'type' => 'string' },
        'uniqueItems' => true,
        'default' => []
      }
    },
    'type' => %w[object boolean],
    'properties' => {
      '$id' => {
        'type' => 'string',
        'format' => 'uri-reference'
      },
      '$schema' => {
        'type' => 'string',
        'format' => 'uri'
      },
      '$ref' => {
        'type' => 'string',
        'format' => 'uri-reference'
      },
      '$comment' => {
        'type' => 'string'
      },
      'title' => {
        'type' => 'string'
      },
      'description' => {
        'type' => 'string'
      },
      'default' => true,
      'readOnly' => {
        'type' => 'boolean',
        'default' => false
      },
      'writeOnly' => {
        'type' => 'boolean',
        'default' => false
      },
      'examples' => {
        'type' => 'array',
        'items' => true
      },
      'multipleOf' => {
        'type' => 'number',
        'exclusiveMinimum' => 0
      },
      'maximum' => {
        'type' => 'number'
      },
      'exclusiveMaximum' => {
        'type' => 'number'
      },
      'minimum' => {
        'type' => 'number'
      },
      'exclusiveMinimum' => {
        'type' => 'number'
      },
      'maxLength' => { '$ref' =>  '#/definitions/nonNegativeInteger' },
      'minLength' => { '$ref' =>  '#/definitions/nonNegativeIntegerDefault0' },
      'pattern' => {
        'type' => 'string',
        'format' => 'regex'
      },
      'additionalItems' => { '$ref' => '#' },
      'items' => {
        'anyOf' => [
          { '$ref' => '#' },
          { '$ref' => '#/definitions/schemaArray' }
        ],
        'default' => true
      },
      'maxItems' => { '$ref' =>  '#/definitions/nonNegativeInteger' },
      'minItems' => { '$ref' =>  '#/definitions/nonNegativeIntegerDefault0' },
      'uniqueItems' => {
        'type' => 'boolean',
        'default' => false
      },
      'contains' => { '$ref' => '#' },
      'maxProperties' => { '$ref' =>  '#/definitions/nonNegativeInteger' },
      'minProperties' => { '$ref' =>  '#/definitions/nonNegativeIntegerDefault0' },
      'required' => { '$ref' => '#/definitions/stringArray' },
      'additionalProperties' => { '$ref' => '#' },
      'definitions' => {
        'type' => 'object',
        'additionalProperties' => { '$ref' => '#' },
        'default' => {}
      },
      'properties' => {
        'type' => 'object',
        'additionalProperties' => { '$ref' => '#' },
        'default' => {}
      },
      'patternProperties' => {
        'type' => 'object',
        'additionalProperties' => { '$ref' => '#' },
        'propertyNames' => { 'format' => 'regex' },
        'default' => {}
      },
      'dependencies' => {
        'type' => 'object',
        'additionalProperties' => {
          'anyOf' => [
            { '$ref' => '#' },
            { '$ref' => '#/definitions/stringArray' }
          ]
        }
      },
      'propertyNames' => { '$ref' => '#' },
      'const' => true,
      'enum' => {
        'type' => 'array',
        'items' => true,
        'minItems' => 1,
        'uniqueItems' => true
      },
      'type' => {
        'anyOf' => [
          { '$ref' => '#/definitions/simpleTypes' },
          {
            'type' => 'array',
            'items' => { '$ref' => '#/definitions/simpleTypes' },
            'minItems' => 1,
            'uniqueItems' => true
          }
        ]
      },
      'format' => {
        'type' => 'string',
        # uri-template, was removed from `enum` to not be supported
        'enum' => %w[
          date-time
          date
          time
          email
          idn-email
          hostname
          idn-hostname
          ipv4
          ipv6
          uri
          uri-reference
          iri
          iri-reference
          json-pointer
          relative-json-pointer
          regex
        ]
      },
      # 'contentMediaType' => { 'type' => 'string' }, # not supported
      # 'contentEncoding' => { 'type' => 'string' }, # not supported
      'if' => { '$ref' => '#' },
      'then' => { '$ref' => '#' },
      'else' => { '$ref' => '#' },
      'allOf' => { '$ref' => '#/definitions/schemaArray' },
      'anyOf' => { '$ref' => '#/definitions/schemaArray' },
      'oneOf' => { '$ref' => '#/definitions/schemaArray' },
      'not' => { '$ref' => '#' },
      # Custom.
      'not_blank' => {
        'type' => 'boolean',
        'default' => false
      },
      'not_zero' => {
        'type' => 'boolean',
        'default' => false
      }
    },
    'default' => true
  }.freeze
end
