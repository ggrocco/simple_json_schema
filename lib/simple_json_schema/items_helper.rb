# frozen_string_literal: true

module SimpleJSONSchema
  module ItemsHelper
    class << self
      def checkers(scope)
        Checker.at_size(scope, :maxItems, :>)
        Checker.at_size(scope, :minItems, :<)
        Checker.unique_items(scope)
      end

      def contains_in_items?(scope, &block)
        contains = scope[:contains]
        return if contains.nil?

        scope.error(:contains) unless scope.value.each_index.any?(&block)
      end

      def each_path(scope)
        items = scope[:items]
        additional_items = scope[:additionalItems]
        many_types = items.is_a?(Array)

        scope.value.each_index do |index|
          if many_types
            if index < items.size
              yield [:items, index], index
            elsif !additional_items.nil?
              yield [:additionalItems], index
            else
              break # protect over big arrays.
            end
          else
            yield [:items], index
          end
        end
      end
    end
  end
end
