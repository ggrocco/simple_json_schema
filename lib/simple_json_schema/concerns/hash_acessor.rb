# frozen_string_literal: true

module SimpleJSONSchema
  module Concerns
    module HashAccessor
      def hash_accessor(hash_name, accessors)
        define_method(hash_name) do
          instance_variable_get("@#{hash_name}") ||
            instance_variable_set("@#{hash_name}", {})
        end

        accessors.each do |name|
          define_method(name) do
            instance_variable_get("@#{hash_name}")[name]
          end

          define_method("#{name}=") do |value|
            instance_variable_get("@#{hash_name}")[name] = value
          end
        end
      end
    end
  end
end
