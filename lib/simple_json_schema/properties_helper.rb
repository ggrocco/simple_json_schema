# frozen_string_literal: true

module SimpleJSONSchema
  module PropertiesHelper
    class << self
      def checkers(scope)
        Checker.required_keys(scope)
        Checker.at_size(scope, :maxProperties, :>)
        Checker.at_size(scope, :minProperties, :<)
      end

      def each_dependency(scope)
        dependencies = scope[:dependencies]
        return unless dependencies.is_a?(Hash)

        value = scope.value
        return unless value.is_a?(Hash)

        dependencies.each_key do |dependency|
          next unless value.key?(dependency)

          yield([:dependencies, dependency])
        end
      end

      def processe_defualt(scope)
        return unless scope.options[:insert_defaults] == true

        properties = scope[:properties]
        return unless properties

        value = scope.value
        properties.each do |property_name, property_schema|
          if !value.key?(property_name) && property_schema.is_a?(Hash) && property_schema.key?('default')
            value[property_name] = property_schema.fetch('default').clone
          end
        end
      end

      def each_property_name(scope, &block)
        property_names = scope[:propertyNames]
        return if property_names.nil?

        scope.value.each_key(&block)
      end

      def map_property_schema_path(scope, property_name)
        paths = []
        paths << [:properties, property_name] if scope[:properties]&.key?(property_name)

        if scope.key?(:patternProperties)
          select_patten_properties(scope, property_name).each do |pattern|
            paths << [:patternProperties, pattern]
          end
        end

        paths << [:additionalProperties] if paths.empty? && scope.key?(:additionalProperties)
        paths
      end

      private

      def select_patten_properties(scope, property_name)
        scope[:patternProperties].keys.select do |pattern|
          RegexHelper.ecma_262_regex(pattern, scope.cache).match?(property_name)
        end
      end
    end
  end
end
