# frozen_string_literal: true

module SimpleJSONSchema
  class Cache
    attr_reader :memory

    def initialize
      @memory = {}
    end

    def fetch(key)
      memory[key] ||= yield
    end
  end
end
