# frozen_string_literal: true

module SimpleJSONSchema
  module URIExtender
    class << self
      def join_uri(base, complement)
        complement = URI.parse(complement) if complement

        if base && complement
          return complement if base.relative? && complement.relative?

          URI.join(base, complement)
        else
          complement || base
        end
      end
    end
  end
end
