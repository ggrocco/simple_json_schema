# frozen_string_literal: true

module SimpleJSONSchema
  module RefHelper
    INTEGER_REGEX = /\A-?\d+\Z/.freeze
    POINTER_SPLIT = %r{(?<!\^)/}.freeze
    POINTER_GSUB = %r{\^[/^]|~[01]}.freeze
    POINTER_ESC = { '^/' => '/', '^^' => '^', '~0' => '~', '~1' => '/' }.freeze
    RefPointer = Struct.new(:ref_paths, :segment, :segment_paths, :parent_uri, keyword_init: true)

    class << self
      def pointer(scope)
        ref = scope[:$ref]
        return nil if ref.nil?

        if ref.start_with?('#')
          local_ref(scope, ref[1..])
        else
          ref_uri = URIExtender.join_uri(scope.parent_uri, ref)
          if (id = scope.loaded_ids[ref_uri.to_s])
            return RefPointer.new(ref_paths: id[:schema_paths], segment: id[:schema], parent_uri: ref_uri)
          end

          uri_ref(scope, ref_uri)
        end
      end

      private

      def local_ref(scope, ref)
        return RefPointer.new unless Checker.json_pointer?(ref)

        paths, parent_uri = path_of_ref(scope.schema, ref)
        RefPointer.new(ref_paths: paths, parent_uri: parent_uri || scope.parent_uri)
      end

      def uri_ref(scope, ref_uri)
        segment = resolver_uri(scope, ref_uri)
        reref_base(segment, scope.schema_paths)
        segment_paths, parent_uri = path_of_ref(segment, ref_uri.fragment) if Checker.json_pointer?(ref_uri.fragment)

        RefPointer.new(ref_paths: scope.schema_paths, segment: segment,
                       segment_paths: segment_paths || [], parent_uri: parent_uri || ref_uri)
      end

      def resolver_uri(scope, uri)
        schema = scope.cache.fetch(uri.to_s) do
          if (resolver = scope.options[:resolver]) && resolver.respond_to?(:call)
            resolver.call(uri)
          else
            JSON.parse(::Net::HTTP.get(uri))
          end
        end

        schema.is_a?(Hash) ? schema.with_indifferent_access : schema
      end

      def path_of_ref(schema, ref)
        paths = decoded_ref(ref)
        parent_uri = pointer_uri(schema, paths)
        [paths, parent_uri]
      end

      def decoded_ref(pointer)
        URI.decode_www_form_component(pointer).split(POINTER_SPLIT).map do |part|
          if INTEGER_REGEX.match?(part)
            part.to_i
          else
            part.gsub(POINTER_GSUB) { |m| POINTER_ESC[m] }
          end
        end.reject(&:blank?)
      end

      def pointer_uri(schema, paths)
        uri_parts = nil

        paths.reduce(schema) do |obj, token|
          next obj.fetch(token.to_i) if obj.is_a?(Array)

          if (obj_id = obj[:$id])
            uri_parts ||= []
            uri_parts << obj_id
          end
          obj.fetch(token)
        end
        uri_parts ? URI.join(*uri_parts) : nil
      end

      def reref_base(schema, paths)
        return if schema.nil? || paths.empty?

        nested_ref_replace_value(schema) do
          "#/#{paths.join('/')}"
        end
      end

      def nested_ref_replace_value(object, &block)
        if object.respond_to?(:key?) && object.key?(:$ref) && object[:$ref].start_with?('#')
          object[:$ref] = yield()
        elsif object.respond_to?(:each)
          r = nil
          object.find { |*a| r = nested_ref_replace_value(a.last, &block) }
          r
        end
      end
    end
  end
end
