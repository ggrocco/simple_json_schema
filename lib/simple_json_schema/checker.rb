# frozen_string_literal: true

module SimpleJSONSchema
  module Checker
    JSON_POINTER_REGEX_STRING = '(\/([^~\/]|~[01])*)*'
    JSON_POINTER_REGEX = /\A#{JSON_POINTER_REGEX_STRING}\z/.freeze
    RELATIVE_JSON_POINTER_REGEX = /\A(0|[1-9]\d*)(#|#{JSON_POINTER_REGEX_STRING})?\z/.freeze

    class << self
      def at_value(scope, check, operation)
        over = scope[check]
        scope.error(check, count: over) if over && scope.value&.public_send(operation, over)
      end

      def at_size(scope, check, operation)
        over = scope[check]
        scope.error(check, count: over) if over && scope.value&.size&.public_send(operation, over)
      end

      def required_keys(scope)
        required = scope[:required]
        return unless required.is_a?(Array)

        keys = scope.value.keys.map(&:to_s)
        missing_keys = required.reject { |require| keys.include?(require) }
        scope.error(:required, missing_keys: missing_keys) if missing_keys.any?
      end

      def unique_items(scope)
        value = scope.value
        scope.error(:uniqueItems) if scope[:uniqueItems] && value.size != value.uniq.size
      end

      def enum(scope)
        enum = scope[:enum]
        scope.error(:enum) if enum && !enum.include?(scope.value)
      end

      def const(scope)
        return if scope.segment.is_a?(Array)

        scope.error(:const) if scope.key?(:const) && scope[:const] != scope.value
      end

      def json_pointer?(value)
        JSON_POINTER_REGEX.match?(value)
      end

      def relative_json_pointer?(value)
        RELATIVE_JSON_POINTER_REGEX.match?(value)
      end
    end
  end
end
