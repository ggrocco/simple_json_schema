# frozen_string_literal: true

require_relative 'lib/simple_json_schema/version'

Gem::Specification.new do |spec|
  spec.name          = 'simple_json_schema'
  spec.version       = SimpleJSONSchema::VERSION
  spec.authors       = ['Georgeo Rocco']
  spec.email         = ['ggrocco@vliper.com']

  spec.description   = 'Simple JSON Schema validator. Supports draft 7 partial,
                          based on https://github.com/davishmcclurg/json_schemer'
  spec.summary       = <<-SUMMARY
    This implementation of the JSON Schema validation is based on the
    https://github.com/davishmcclurg/json_schemer) (whit is the recommended to be used),
    but only implement the Draft7
  SUMMARY
  spec.homepage      = 'https://gitlab.com/vliper/simple_json_schema'
  spec.license       = 'MIT'

  spec.required_ruby_version = Gem::Requirement.new('>= 2.6.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport', '~> 6.0'
  spec.add_dependency 'addressable', '~> 2.7'
  spec.add_dependency 'ecma-re-validator', '~> 0.3'
  spec.add_dependency 'regexp_parser', '~> 2.0'

  spec.add_development_dependency 'pry-byebug', '~> 3.9'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rspec', '~> 3.10'
  spec.add_development_dependency 'rubocop', '~> 1.6'
  spec.add_development_dependency 'rubocop-performance', '~> 1.9'
  spec.add_development_dependency 'rubocop-rspec', '~> 2.1'
  spec.add_development_dependency 'simplecov', '~> 0.20'
end
