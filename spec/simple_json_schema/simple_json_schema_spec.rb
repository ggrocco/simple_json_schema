# frozen_string_literal: true

RSpec.describe SimpleJSONSchema do
  describe 'hooks' do
    let(:data) { { number: 2 } }
    let(:schema) { { properties: { number: { type: 'integer', minimum: 3 } } } }
    let(:options) do
      {
        before_property_validation: ->(scope) { scope.value *= 2 },
        after_property_validation: ->(scope) { scope.value = 'pass' }
      }
    end

    it { expect(described_class.valid?(data, schema, options)).to be(true) }

    it 'after hook change the value' do
      described_class.valid?(data, schema, options)
      expect(data[:number]).to be('pass')
    end
  end
end
