# frozen_string_literal: true

RSpec.shared_examples 'json_custom_test_suite' do
  extend JSONExampleTest

  files = Dir["#{__dir__}/json_custom_test_suite/**/*.json"]
  files.each do |file|
    test_example_file(file)
  end
end

RSpec.describe 'JSONCustomTestSuite' do
  include_examples 'json_custom_test_suite'
end
