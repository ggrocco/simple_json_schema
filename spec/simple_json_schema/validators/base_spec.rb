# frozen_string_literal: true

RSpec.describe SimpleJSONSchema::Validators::Base do
  subject(:base) { described_class.new }

  let(:scope) { SimpleJSONSchema::Scope.new }

  it 'require to be implemented' do
    expect { base.valid(scope) }.to raise_error(NotImplementedError)
  end
end
