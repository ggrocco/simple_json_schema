# frozen_string_literal: true

require 'json'
require 'pathname'

RSpec.shared_examples 'json_schema_test_suite' do |draft_version, not_implemented|
  extend JSONExampleTest

  files = Dir["spec/json-schema-test-suite/tests/#{draft_version}/**/*.json"]
  files.reject { |f| not_implemented.any? { |n| f.include?(n) } }
       .each do |file|
    test_example_file(file, file[/#{draft_version}.*/], options: { resolver: proc do |uri|
      if uri.host == 'localhost'
        # Resolve localhost test schemas
        path = Pathname.new(__dir__).join('..', 'json-schema-test-suite', 'remotes', uri.path.delete_prefix('/'))
        JSON.parse(path.read)
      else
        JSON.parse(::Net::HTTP.get(uri))
      end
    end })
  end
end

RSpec.describe 'JSONSchemaTestSuite' do
  include_examples 'json_schema_test_suite', 'draft7',
                   # not implementend features on Draft7
                   %w[
                     optional/content.json
                     optional/format/uri-template.json
                   ]
end
