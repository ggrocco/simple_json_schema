# frozen_string_literal: true

require 'json'
module JSONExampleTest
  def test_example_file(file, content_text = nil, options: nil)
    context content_text || file.split('/').last do
      read_exemples(file).each do |example|
        describe example.fetch('description') do
          example.fetch('tests').each do |test|
            test_example(example, test, options)
          end
        end
      end
    end
  end

  private

  def read_exemples(file)
    JSON.parse(File.read(file))
  end

  def test_example(example, test, options)
    schema = example.fetch('schema')
    data = test.fetch('data')

    expect_test(test, data, schema, options)
  end

  # rubocop:disable RSpec/MultipleExpectations, RSpec/ExampleLength, Metrics/AbcSize
  def expect_test(test, data, schema, options)
    validate = SimpleJSONSchema.valid(data, schema, options)
    valid = validate.none?
    fail_message = "'#{data.to_json}' was not valid for '#{schema.to_json}'"
    fail_message += "\n\nerrors: #{validate}" unless test.fetch('valid') == valid

    it test.fetch('description') do
      if test.fetch('valid')
        expect(valid).to be(true), fail_message
      else
        expect(valid).to be(false), fail_message
      end

      if (has_include = test['include'])
        if has_include.is_a?(Hash)
          expect(data).to include(has_include)
        else
          expect(data).to include(*has_include)
        end
      end
    end
  end
  # rubocop:enable RSpec/MultipleExpectations, RSpec/ExampleLength, Metrics/AbcSize
end
