# SimpleJSONSchema

[![pipeline status](https://gitlab.com/vliper/simple_json_schema/badges/master/pipeline.svg)](https://gitlab.com/vliper/simple_json_schema/-/commits/master)
[![coverage report](https://gitlab.com/vliper/simple_json_schema/badges/master/coverage.svg)](https://gitlab.com/vliper/simple_json_schema/-/commits/master)
[![Gem Version](https://badge.fury.io/rb/simple_json_schema.svg)](https://badge.fury.io/rb/simple_json_schema)

This implementation of the JSON Schema validation is based on the [JSONSchemer](https://github.com/davishmcclurg/json_schemer) **(WHIT IS THE RECOMMENDED TO BE USED!)**, but only implement the Draft7, whit [limitations](https://gitlab.com/vliper/simple_json_schema/-/blob/master/spec/simple_json_schema/json_schema_test_suite_spec.rb#L26) and some feature embedded.

# Installation

Add this line to your application's Gemfile:

```ruby
gem 'simple_json_schema'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install simple_json_schema

## Usage
The use is based on two methods, the 'valid' and 'valid?'.

```ruby

require 'simple_json_schema'

schema = {
  type: 'object',
  properties: {
    abc: {
      type: 'integer',
      minimum: 11
    }
  }
  options: { cast: true } # passing options whit the schema.
}

# true/false validation
SimpleJSONSchema.valid?({ 'abc' => 11 }, schema)
# => true

SimpleJSONSchema.valid?({ 'abc' => 10 }, schema)
# => false

# using options, with can be at the schema or passing as parameter.
# SimpleJSONSchema.valid?({ 'abc' => '10' }, schema, { cast: true } )
SimpleJSONSchema.valid?({ 'abc' => '11' }, schema)
# => true

# Return the errors if any.
SimpleJSONSchema.valid({ 'abc' => 10 }, schema)
# => [{:type=>:minimum, :segment=>{"type"=>"integer", "minimum"=>11}, :value=>10, :data_pointer=>"/abc", :schema_pointer=>"/properties/abc"}]
```

#### Options:
Whit options is possible to improve the behavior.

| Name | Description | Default | Allowed values |
| ---- | ----------- | ------- | -------------- |
| after_property_validation | Call after validate a Property | nil | Proc to receive Scope object |
| before_property_validation | Call before validate a Property | nil | Proc to receive Scope object |
| cache | The object to do the cache for schema ref and regex | Cache class per execution | any object whit implement fetch(name) { callback } |
| cast | Cast the values | false | true/false |
| insert_defaults | Insert the default property if not defined at the validation | false | true/false |

#### Extra attributes
Some extra implementations on types (THIS IS NOT ON THE DRAFT7 DEFINITION!)

| Type | Attribute | Description | Allowed values |
| ---- | --------- | ----------- | -------------- |
| string | not_blank | Will evaluate the `blank?` concept on the string | true/false |
| integer | not_zero | Will enforce value != 0 | true/false |
| number | not_zero | Will enforce value != 0.0 | true/false |

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/ggrocco/simple_json_schema. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/ggrocco/simple_json_schema/blob/master/CODE_OF_CONDUCT.md).


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Simple::Json::Schema project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/ggrocco/simple_json_schema/blob/master/CODE_OF_CONDUCT.md).
